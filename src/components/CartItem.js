const CartItem = ({ item, removeItem }) => {
  const { image, price, title, quantity } = item;
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '0 20px',
        maxWidth: '800px',
        width: '100%'
      }}
    >
      <div style={{ width: '150px', height: '120px', margin: '10px 0' }}>
        <img
          src={image}
          alt="cart item"
          style={{ height: '100px', objectFit: 'cover' }}
        />
      </div>
      <p>{title}</p>
      <p>{price}</p>
      <p>x {quantity}</p>
      <button
        style={{ fontSize: '24px', lineHeight: '1' }}
        onClick={() => removeItem(item)}
      >
        {' '}
        -{' '}
      </button>
    </div>
  );
};

export default CartItem;
