import { useEffect, useState } from 'react';
import Cart from './Cart';
import Store from './Store';
import Summary from './Summary';

const StoreContainer = () => {
  const [items, setItems] = useState([]);
  const [index, setIndex] = useState(0);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    getItems();
  }, []);

  const getItems = async () => {
    const response = await fetch('https://fakestoreapi.com/products');
    const result = await response.json();
    setItems(result);
  };

  const addItemToCart = (item) => {
    const updatedCart = [...cart, item];
    setCart(updatedCart);
  };

  const removeItemFromCart = (item) => {
    const itemIndexToRemove = cart.findIndex(
      (cartItem) => cartItem.id === item.id
    );
    let cartCopy = [...cart];
    cartCopy.splice(itemIndexToRemove, 1);
    setCart(cartCopy);
  };

  const setCurrentItemIndex = (index) => {
    setIndex(index);
  };

  return (
    <div>
      <Store
        items={items}
        index={index}
        setCurrentItemIndex={setCurrentItemIndex}
        setCart={setCart}
        addItemToCart={addItemToCart}
      />
      <Cart cart={cart} removeItemFromCart={removeItemFromCart} />
      <Summary cart={cart} />
    </div>
  );
};

export default StoreContainer;
