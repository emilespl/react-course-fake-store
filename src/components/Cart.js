import CartItem from './CartItem';

const Cart = ({ cart, removeItemFromCart }) => {
  const uniqueItems = [...new Set(cart)];

  const uniqueItemsWithCount = uniqueItems.map((item) => {
    return {
      ...item,
      quantity: cart.filter((cartItem) => cartItem.id === item.id).length
    };
  });

  const renderCartItems = uniqueItemsWithCount.map((item, index) => (
    <CartItem item={item} key={index} removeItem={removeItemFromCart} />
  ));
  return (
    <div>
      <h1>Cart</h1>
      <div
        style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap' }}
      >
        {renderCartItems}
      </div>
    </div>
  );
};

export default Cart;
