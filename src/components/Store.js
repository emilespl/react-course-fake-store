const Store = ({ items, index, setCurrentItemIndex, addItemToCart }) => {
  const item = items.length > 0 && items[index];
  const { image, price, title } = item;

  const next = () => {
    if (index < items.length - 1) {
      const nextIndex = ++index;
      setCurrentItemIndex(nextIndex);
    }
  };

  const previous = () => {
    if (index > 0) {
      const previousIndex = --index;
      setCurrentItemIndex(previousIndex);
    }
  };
  return (
    <div>
      <div
        style={{
          height: '450px',
          width: '400px',
          marginRight: 'auto',
          marginLeft: 'auto',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        <i
          className={`arrow left ${index > 0 ? '' : 'inactive'}`}
          onClick={() => previous()}
        ></i>
        <img src={image} style={{ width: '250px' }} alt="product" />
        <i
          className={`arrow right ${
            index < items.length - 1 ? '' : 'inactive'
          }`}
          onClick={() => next()}
        ></i>
      </div>
      <h2>{title}</h2>
      <h3>Price: {price}</h3>
      <button onClick={() => addItemToCart(item)}> Add to cart</button>
    </div>
  );
};

export default Store;
