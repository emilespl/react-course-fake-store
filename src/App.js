import './App.css';
import StoreContainer from './components/StoreContainer';

function App() {
  return (
    <div className="App">
      <StoreContainer />
    </div>
  );
}

export default App;
